# @ignore
# @debug
@parallel=false
Feature: Tests for the home page

    Background: Define URL
        Given url apiUrl

    Scenario: Get all tags
        And path 'tags'
        When method Get
        Then status 200
        And match response.tags == "#array"
        And match each response.tags == "#string"

    Scenario: Get 10 articles from the page
        * def timeValidator = read('classpath:helpers/timeValidator.js')

        And path 'articles'
        # And  param limit = 10
        # And param offset = 0
        And params {limit : 10, offset: 0}
        When method Get
        Then status 200
        # And match response.articles == '#[10]'
        # And match response.articlesCount == 6
        And match response ==
            """
            {
                "articles": "#array",
                "articlesCount": "#number"
            }
            """
        # And match response.articles[0].createdAt contains '2022'
        # And match response..bio contains null
        # And match each response..following == "#boolean"
        # And match each response.articles[*].favoritesCount == "#number"
        # And match each response..bio == "##string"
        And match each response.articles ==
            """
            {
                "slug": "#string",
                "title": "#string",
                "description": "#string",
                "body": "#string",
                "tagList": "#array",
                "createdAt": "#? timeValidator(_)",
                "updatedAt": "#? timeValidator(_)",
                "favorited": "#boolean",
                "favoritesCount": "#number",
                "author": {
                    "username": "#string",
                    "bio": "##string",
                    "image": "#string",
                    "following": "#boolean"
                }
            }
            """


    Scenario: Conditional logic
        Given path 'articles'
        And params {limit : 10, offset: 0}
        When method Get
        Then status 200
        * def favoritesCount = response.articles[0].favoritesCount
        * def article = response.articles[0]

        # * if (favoritesCount == 0) karate.call('classpath:helpers/AddLikes.feature',article)

        * def result = favoritesCount == 0 ? karate.call('classpath:helpers/AddLikes.feature',article).likesCount : favoritesCount

        Given path 'articles'
        And params {limit : 10, offset: 0}
        When method Get
        Then status 200
        And match response.articles[0].favoritesCount == result

    @debug
    Scenario: Retry call
        * configure retry = {count: 10, interval: 50}

        Given path 'articles'
        And params {limit : 10, offset: 0}
        And retry until response.articles[0].favoritesCount == 1 || response.articles[0].favoritesCount ==0
        When method Get
        Then status 200

    Scenario: Sleep call
        * def sleep = function(pause){java.lang.Thread.sleep(pause)}

        Given path 'articles'
        And params {limit : 10, offset: 0}
        When method Get
        * eval sleep(5000)
        Then status 200

    Scenario: Number to string
        * def foo = 10
        * def json = {"bar": #(foo+'')}
        * match json == {"bar": '10'}


    Scenario: String to number
        * def foo = '10'
        #* def json = {"bar": #(foo*1)}
        * def json = {"bar": #(parseInt(foo))}
        * match json == {"bar": 10}