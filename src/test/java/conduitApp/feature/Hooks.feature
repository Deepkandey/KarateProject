#@debug
# @parallel=false
Feature: Hooks

    Background: hooks
        # call (before Scenario level), callonce(before Feature level) and callSingle(before all)
        # afterScenario (after Scenario level), afterFeature (after Feature level)
        * def result = callonce read('classpath:helpers/Dummy.feature')
        * def username = result.username

        #after hooks
        * configure afterScenario = function(){karate.call('classpath:helpers/Dummy.feature')}
        * configure afterFeature =
            """
            function(){
            karate.log('After Feature Text')
            }
            """


    Scenario: First Scenario
        * print 'Before Scenario username: ', username
        * print 'This is first scenario'

    Scenario: Second Scenario
        * print 'Before Scenario username: ', username
        * print 'This is second scenario'