# @ignore
Feature: Sign up new user

    Background: Preconditions
        * def dataGenerator = Java.type("helpers.DataGenerator");
        * def randomEmail = dataGenerator.getRandomEmail()
        * def randomUsername = dataGenerator.getRandomUsername()
        Given url apiUrl

    Scenario: New user Sign up
        * def jsFunction =
            """
            function () {
            var DataGenerator = Java.type("helpers.DataGenerator");
            var generator = new DataGenerator();
            return generator.getRandomPassword();
            }
            """

        * def randomPassword = call jsFunction

        Given path 'users'
        And request
            """
            {
            "user": {
            "username": #(randomUsername),
            "email": #(randomEmail),
            "password": #(randomPassword)
            }
            }
            """
        When method Post
        Then status 200
        And match response.user.email == randomEmail
        And match response.user.username == randomUsername
        And match response.user.token == "#notnull"

    Scenario Outline: Validate <TC_name> sign up error message
        Given path 'users'
        And request
            """
            {
                "user": {
                    "username": "<username>",
                    "email": "<email>",
                    "password": "123Karate123"
                }
            }
            """

        When method Post
        Then status 422
        And match response == <errorResponse>

        Examples:
            | TC_name                | username          | email            | errorResponse                                          |
            | username_already_taken | Oliver Mooney     | #(randomEmail)   | {"errors": {"username": ["has already been taken"] } } |
            | email_already_taken    | #(randomUsername) | andrew2@test.com | {"errors": {"email": ["has already been taken"] } }    |
            | email_can't be blank   | #(randomUsername) |                  | {"errors": {"email": ["can't be blank"] } }            |