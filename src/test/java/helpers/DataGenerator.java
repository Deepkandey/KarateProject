package helpers;

import java.util.Locale;

import com.devskiller.jfairy.Fairy;

import net.minidev.json.JSONObject;

public class DataGenerator {

    public static String getRandomEmail() {
        Fairy fairy = Fairy.create(new Locale("en", "IN"));
        String email = fairy.person().getFirstName().toLowerCase() + fairy.baseProducer().randomBetween(0, 100)
                + "@test.com";
        return email;
    }

    public static String getRandomUsername() {
        Fairy fairy = Fairy.create(new Locale("en", "IN"));
        String username = fairy.person().getFullName();
        return username;
    }

    public String getRandomPassword() {
        Fairy fairy = Fairy.create(new Locale("en", "IN"));
        String password = fairy.baseProducer().randomElement("123Karate@abc");
        return password;
    }

    public static JSONObject getRandomArticleValues() {
        Fairy fairy = Fairy.create(new Locale("en", "IN"));
        String title = fairy.textProducer().randomString(10);
        String description = fairy.textProducer().word();
        String body = fairy.textProducer().sentence();
        JSONObject json = new JSONObject();
        json.put("title", title);
        json.put("description", description);
        json.put("body", body);
        return json;
    }
}
