function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log("karate.env system property was:", env);
  if (!env) {
    env = "dev";
  }
  var config = {
    apiUrl: "https://api.realworld.io/api",
  };
  if (env == "dev") {
    config.userEmail = "karate13e@test.com";
    config.userPassword = "Karate123d";
  } else if (env == "qa") {
    config.userEmail = "karate12qa@test.com";
    config.userPassword = "Karate123qa";
  }

  var accessToken = karate.callSingle(
    "classpath:helpers/CreateToken.feature",
    config
  ).authToken;
  karate.configure("headers", { Authorization: "Token " + accessToken });

  return config;
}
